import { Sequelize, Model, DataTypes } from "sequelize";
import getUser from "./user";

export const sequelize = new Sequelize("students", "postgres", "password", {
  host: "localhost",
  dialect: "postgres",
});

export const User = getUser(sequelize);
