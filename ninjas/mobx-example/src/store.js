import { useLocalStore } from "mobx-react";
import { v4 as uuidv4 } from "uuid";
import { observable, action } from "mobx";

const createTodo = (label) => {
  return observable(
    {
      label,
      id: uuidv4(),
      completed: false,
      toggle() {
        this.completed = !this.completed;
      },
    },
    {
      toggle: action,
    }
  );
};

const createPerson = (person) => {
  return observable(
    {
      ...person,
      id: uuidv4(),
      sayHi() {
        alert(`Hello my name is ${this.name}`);
      },
    },
    {
      sayHi: action,
    }
  );
};

const useStore = () => {
  const store = useLocalStore(() => {
    return {
      currentFilter: "all",
      get currentTodos() {
        switch (store.currentFilter) {
          case "completed":
            return store.todos.filter((todo) => todo.completed);
          case "not-completed":
            return store.todos.filter((todo) => !todo.completed);
          default:
            return store.todos;
        }
      },
      people: [],
      todos: [createTodo("Gabby")],
      addTodo(label) {
        store.todos.push(createTodo(label));
      },
      setPeople(people) {
        store.people = people;
      },
      changeFilter(filter) {
        store.currentFilter = filter;
      },
      async makeNetworkRequest() {
        try {
          const res = await fetch("https://swapi.dev/api/people");
          const { results: people } = await res.json();
          store.setPeople(people.map((person) => createPerson(person)));
        } catch (err) {
          console.error("unable to fetch swapi people", err);
        }
      },
    };
  });

  return store;
};

export default useStore;
