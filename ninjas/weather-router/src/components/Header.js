import React from "react";
import { Segment } from "@fluentui/react-northstar";
import { Link } from "react-router-dom";

export const Header = () => (
  <Segment
    color="brand"
    inverted
    styles={{
      gridColumn: "span 4",
    }}
  >
    <>
      <Link to="/">HOME</Link>
      {" | "}
      <Link to="/filtered">Filtered</Link>
    </>
  </Segment>
);
