import React from "react";
import { useParams, Redirect } from "react-router-dom";

export const LocationByIdScreen = ({ locations }) => {
  const params = useParams();
  const location = locations.find((loc) => loc.id === params.filterId);
  if (!location) {
    console.log("Unable to find that place.");
    return <Redirect to="/filtered" />;
  }
  return <>{JSON.stringify(location)}</>;
};
